from scapy.packet import *
from scapy.fields import *
from scapy.layers.inet import *

import enip_tcp

class PCCC(Packet):
    name = "PCCC"
    fields_desc = [
        XByteField("cmd", 0x00),
        XByteField("STS", 0x00),
        XShortField("transaction_num", 0x0000),
        XByteField("function_code", 0x00)
    ]

bind_layers(enip_tcp.ENIP_SendUnitData_Item, PCCC, type_id=0x0091)

    

