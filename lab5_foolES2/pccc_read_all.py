import sys,os
sys.path.append('/home/hyunguk/csci6627/scapy_enip')
import struct
import argparse

from scapy.all import *
from enip_tcp import *
import plc


class PCCC_client():
    def __init__(self, plc_ip):
        self.tranID = 0
        self.session = plc.PLCClient(plc_ip)
        self.plc_ip = plc_ip

    def send_recv_msg(self, pccc_msg):
        # correct transaction ID
        pccc_msg = bytearray(pccc_msg)
        pccc_msg[2:4] = struct.pack(">H", self.tranID)
        self.tranID = (self.tranID + 4) % 65536

        recv_buf = self.session.send_pccc(bytes(pccc_msg))
        
        return recv_buf
         

    def send_pccc_read_req(self, size, file_num, file_type, element_num, sub_element_num):
        read_req_msg = '\x0f\x00\x00\x00\xa2' + struct.pack("B", size) + struct.pack("B", file_num) + struct.pack("B", file_type) 

        if element_num < 255:
            read_req_msg += struct.pack("B", element_num)
        else:
            read_req_msg += '\xff' + struct.pack("<H", element_num)
         
        if sub_element_num < 255:
            read_req_msg += struct.pack("B", sub_element_num)
        else:
            read_req_msg += '\xff' + struct.pack("<H", sub_element_num)


        recv = ENIP_TCP(self.send_recv_msg(read_req_msg))
        pccc_res = bytes(recv[ENIP_SendRRData].items[1].payload)

        return pccc_res
      
    
    def read_all_sub_element(self, file_num, file_type, element_num):
        buf = ''
        
        sub_element_num = 0
        max_read_size = 80

        
        while (sub_element_num*2) + max_read_size <= 65536:
            pccc_res = self.send_pccc_read_req(max_read_size, file_num, file_type, element_num, sub_element_num)
            
            if pccc_res[0:2] == '\x4f\x10':     # error reply
                if sub_element_num == 0:        # the very first request
                    return None
                else:
                    for i in range(max_read_size-2,1,-2):
                        pccc_res = self.send_pccc_read_req(i, file_num, file_type, element_num, sub_element_num)
                        
                        if pccc_res[0:2] != '\x4f\x10':
                            return buf + pccc_res[4:]
            
                    return buf

            else:
                buf += pccc_res[4:]
                sub_element_num += (max_read_size / 2)
        
        return buf

def main():
    parser = argparse.ArgumentParser(description="Read all the address space of the PCCC protocol")
    parser.add_argument("plc_ip", help="IP address of the target PLC")
    parser.add_argument("output_dir", help="output directory")
    args = parser.parse_args()

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    pccc_client = PCCC_client(args.plc_ip)


    for file_num in range(256):
        for file_type in range(256):
#            for element_num in range(65536):
            element_num = 0
            recv_data = pccc_client.read_all_sub_element(file_num,file_type,element_num)
            if recv_data != None:
                filePath = args.output_dir + "/" + str(file_num) + "_" + str(file_type) + "_" + str(element_num)
                f = open(filePath, "w")
                f.write(recv_data)
                f.close()

if __name__ == '__main__':
    main()
        

