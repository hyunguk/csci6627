import sys,os
sys.path.append('/home/hyunguk/csci6627/lab5_foolES2')
import binascii
from pccc_read_all import *

def main():
    parser = argparse.ArgumentParser(description="Read a file of MicroLogix 1400 PLC program")
    parser.add_argument("plc_ip", help="IP address of the target PLC")
    parser.add_argument("output_dir", help="output directory")
    args = parser.parse_args()

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    pccc_client = PCCC_client(args.plc_ip)

    element_num = 0
    file_num = 0x00              
    file_type = 0x03        # Configuration file (File Num:0x00, File Type:0x03)
    recv_data = pccc_client.read_all_sub_element(file_num,file_type,element_num)
    if recv_data != None:
        filePath = args.output_dir + "/" + str(file_num) + "_" + str(file_type) + "_" + str(element_num)
        f = open(filePath, "w")
        f.write(recv_data)
        f.close()

    for i in range(0x66, len(recv_data), 10):   # 0x66 is the offset in the conf file where 10-byte tuples of each file start
        print binascii.hexlify(recv_data[i:i+10])


if __name__ == '__main__':
    main()
