import sys
sys.path.append('/home/ics/csci6627/scapy_enip')

from enip_tcp import *
from scapy.all import *
import binascii
import plc
import struct
import argparse
import sys,os

class PCCC_client():
    def __init__(self, plc_ip):
        self.tranID = 0
        self.session = plc.PLCClient(plc_ip)    # Establish a Ethernet/IP session

    def send_recv_msg(self, pccc_msg):
        # Update transaction identifier
        self.tranID = (self.tranID + 4) % 65536

        # Send PCCC message
        recv_buf = self.session.send_pccc(pccc_msg)

        enip_recv = ENIP_TCP(recv_buf)
        pccc_recv = bytes(enip_recv[ENIP_SendRRData].items[1].payload)

        return pccc_recv
 

    def build_pccc_msg(self, cmd, sts, fnc, size, file_num, file_type, element_num, sub_element_num, data):
        pccc_msg = cmd + sts + struct.pack(">H", self.tranID) + fnc

        if size != None:
            pccc_msg += size

        if file_num != None:
            pccc_msg += self.encode_dynamic_length_field(file_num)

        if file_type != None:
            pccc_msg += file_type

        if element_num != None:
            pccc_msg += self.encode_dynamic_length_field(element_num)

        if sub_element_num != None:
            pccc_msg += self.encode_dynamic_length_field(sub_element_num)

        if data != None:
            pccc_msg += data

        return pccc_msg


    def encode_dynamic_length_field(self, value):
        if value < 255:
            return struct.pack("B", value)
        else:
            return '\xff' + struct.pack("<H", value)         


def parse_pccc_msg_file(file_name):
    f = open(file_name, "r")
    lines = f.readlines()

    cmd=sts=fnc=size=file_num=file_type=element_num=sub_element_num=data=None

    for line in lines:
        l = line.replace(" ", "").split("=")
        if len(l) >= 2:
            field = l[0]
            value = l[1].split("#")[0]
            if field == "CMD":
                cmd = value
            elif field == "STS":
                sts = value
            elif field == "FNC":
                fnc = value
            elif field == "SIZE":
                size = value
            elif field == "FNUM":
                file_num = value
            elif field == "FTYPE":
                file_type = value
            elif field == "ELN":
                element_num = value
            elif field == "SELN":
                sub_element_num = value
            elif field == "DATA":
                data = value

    # CMD, STS, FNC fields are madatory in a PCCC message
    cmd = binascii.unhexlify(cmd)
    sts = binascii.unhexlify(sts)
    fnc = binascii.unhexlify(fnc)

    if size != None:
        size = binascii.unhexlify(size)
    
    if file_num != None:
        file_num = int(file_num, 16)

    if file_type != None:
        file_type = binascii.unhexlify(file_type)

    if element_num != None:
        element_num = int(element_num, 16)

    if sub_element_num != None:
        sub_element_num = int(sub_element_num, 16)

    if data != None:
        data = binascii.unhexlify(data.replace(":",""))
        
    return cmd,sts,fnc,size,file_num,file_type,element_num,sub_element_num,data

def main():
    parser = argparse.ArgumentParser(description="Send a PCCC message")
    parser.add_argument("target_ip", help="IP address to send a PCCC message")
    parser.add_argument("pccc_msg", help="Configuration of PCCC message to send")
    args = parser.parse_args()


    pccc_client = PCCC_client(args.target_ip)
    cmd, sts, fnc, size, file_num, file_type, element_num, sub_element_num, data = parse_pccc_msg_file(args.pccc_msg)
    pccc_msg = pccc_client.build_pccc_msg(cmd, sts, fnc, size, file_num, file_type, element_num, sub_element_num, data)
    recv_msg = pccc_client.send_recv_msg(pccc_msg)

    print "--> " + binascii.hexlify(pccc_msg)   # Sent PCCC message
    print "<-- " + binascii.hexlify(recv_msg)   # Received PCCC_message
    

if __name__ == '__main__':
    main()
        

