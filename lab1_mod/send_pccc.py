import socket
import struct
import binascii

def establish_enip_session(sock):
    """ Establish ENIP Session and return Session Handle"""
    # ENIP header
    cmd = '\x65\x00'         # cmd = 0x0065 (Register Session)
    len_cpd = ''             # Size (in bytes) of command specifc data (CPD)
    s_handle = '\x00' * 4    # ENIPsession handle
    status = '\x00' * 4      # Always 0
    sender_ctx = '\x00' * 8  # Always 0
    option = '\x00' * 4      # Always 0   

    # ENIP Command Specific Data (CPD) for 'Register Session (cmd=0x0065)'
    proto_ver = '\x01\x00'        # protocol version = 1
    option_flags = '\x00' * 2     # Always 0

    cpd = proto_ver + option_flags         # Make Command specific data

    len_cpd = struct.pack("<H", len(cpd))  # <: little endian, H: unsigned short (2 bytes)

    header = cmd + len_cpd + s_handle + status + sender_ctx + option  # Make ENIP Header

    enip_msg = header + cpd                # Make ENIP Message (Register Session Request)

    sock.send(enip_msg)                    # Send a packet  
    recv_buf = sock.recv(1000)             # Received TCP payload (which is ENIP response message) 
    
    session_handle = recv_buf[4:8]         # Extract the Session Handle value

    return session_handle


def enip_sendRRdata_request(sock, session_handle, pccc_msg):
    """ Send 'Send RR Data request' Message to the PLC"""
    # ENIP header
    cmd = '\x6f\x00'         # cmd = 0x006f (Send RR Data)
    len_cpd = ''            # Size (in bytes) of command specifc data (CPD)
    s_handle = session_handle    # ENIPsession handle
    status = '\x00' * 4      # Always 0
    sender_ctx = '\x00' * 8  # Always 0
    option = '\x00' * 4      # Always 0   

    # ENIP Command Specific Data (CPD) for 'Send RR Data (cmd=0x006f)'
    interface_handle = '\x00' * 4
    timeout = '\x00' * 2
    item_count = '\x02\x00'

    # item1
    item1_typeID = '\x81\x00'
    item1_data = '\x00'
    item1_len = struct.pack("<H", len(item1_data))
    item1 = item1_typeID + item1_len + item1_data

    #item2
    item2_typeID = '\x91\x00'
    item2_data = pccc_msg       # IMPORTANT
    item2_len = struct.pack("<H", len(item2_data))
    item2 = item2_typeID + item2_len + item2_data

    cpd = interface_handle + timeout + item_count + item1 + item2         # Make Command specific data

    len_cpd = struct.pack("<H", len(cpd))  # <: little endian, H: unsigned short (2 bytes)

    header = cmd + len_cpd + s_handle + status + sender_ctx + option  # Make ENIP Header

    enip_msg = header + cpd                # Make ENIP Message (Register Session Request)

    sock.send(enip_msg)                    # Send a packet  
    recv_buf = sock.recv(1000)             # Received TCP payload (which is ENIP response message) 
    return recv_buf

def build_pccc_msg():
    """ Build PCCC Request Message """
    # CMD + Status + Transaction ID + Function Code + Data size + File number + file type + element number + sub_element number + DATA

    cmd = '\x0f'    # CMD = 0xf && FNC = 0xaa : Write Request
    status = '\x00'
    transaction_id = '\x00\x00'     # Increase by 4 for each req/res pair
    function_code = '\xaa'  # Write request (CMD=0x0f && function code = 0xaa)
#    function_code = '\xa2'  # Read request (CMD=0x0f && function code = 0xa2)
#    size = '\x06'       # 24 bytes (4 timers * 6 bytes)    
    file_num = '\x04'   # Bit(B3) file num = 0x03, Timer(T4) file num = 0x04
    file_type = '\x86'  # Bit(B3) file type = 0x85 : Timer(T4) file type = 0x86
    element_num = '\x01'    # 0 for B3 data file / Timer object number for T4 data file (0: Timer 0, 1: Timer 1, etc.)
    sub_element_num = '\x01'  # Word (2-bytes) offset within a file 
    pccc_data = '\x64\x00'  # B3:0/0 = 1 (START the operation) / B3:0/0 = 0 (STOP the operation)     

    size = struct.pack("B", len(pccc_data))

    pccc_msg = cmd + status + transaction_id + function_code + size + file_num + file_type + element_num + sub_element_num + pccc_data
#    pccc_msg = cmd + status + transaction_id + function_code + size + file_num + file_type + element_num + sub_element_num

    return pccc_msg


def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)    # AF_INET: IPv4, SOCK_STREAM: TCP
    sock.connect(("137.30.122.151", 44818))                          # Establish TCP/IP session

    session_handle = establish_enip_session(sock)               # Establish ENIP session and get Session Handle

    print "Session Handle: ", binascii.hexlify(session_handle)  # Print Session Handle value as a ASCII string 

    pccc_msg = build_pccc_msg()  
    reply_msg = enip_sendRRdata_request(sock, session_handle, pccc_msg)
    
    print "Reply msg: ", binascii.hexlify(reply_msg)

if __name__ == '__main__':
    main()
